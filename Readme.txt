RELEASE NOTES for Minesweeper

RELEASE DATE:  11/01/2016
BUILD VERSION: 0.01
ENVIRONMENT: 
		Intel® Core™ i5-5250U CPU @ 1.60GHz × 4
		12Gb RAM
		Intel® Broadwell Graphics
		Resolution: 1366 x 768
		OS Ubuntu 14.04 LTS
		Integrated video card
		OpenJDK Java 7 Runtime

IMPLEMENTED FEATURES:
+ 3 game modes: "Новичок", "Любитель", "Профессионал"
+ Best results list ("Чемпионы")
+ Gameplay improved
+ "About" information

KNOWN ISSUES (id, summary):
STP-190 Game screen: inappropriate "New" button image when mouse is pressed
STP-191	New game starts without a confirmation
STP-192	Incorrect format of "Flags" inscription
STP-193	Incorrect Time format in Header
STP-194	Game Field: First uncovered tile may be mined
STP-195	Player lost: Tiles around the Blowing Tile become numbered
STP-196	Player lost: user can put flags on covered tiles
STP-197	Results: Name input is not required
STP-198	Results screen: Only best result for each board provided
STP-199	Results screen: Date of games required
STP-200	Results screen: The Time is represented in seconds
STP-201	Results screen: Results are cleared after application restart
STP-202	Results screen: Quit button is missing
STP-203	Results screen: "New Game" button is missing
STP-204	"Custom game" button is absent
STP-205	Pause button is missing
STP-206	Main Menu is missing
STP-207	Level "Новичок" has incorrect board size

